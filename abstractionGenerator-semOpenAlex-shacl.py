#! /usr/bin/env python3

import rdflib

g = rdflib.Graph()
g.bind("rdf", rdflib.namespace.RDF)
g.bind("rdfs", rdflib.namespace.RDFS)
g.bind("owl", rdflib.namespace.OWL)
g.bind("xsd", rdflib.namespace.XSD)
g.bind("void", rdflib.namespace.VOID)

g.bind("dcterms", rdflib.Namespace("http://purl.org/dc/terms/"))
g.bind("foaf", rdflib.Namespace("http://xmlns.com/foaf/0.1/"))
g.bind("sh", rdflib.Namespace("http://www.w3.org/ns/shacl#"))
g.bind("skos", rdflib.namespace.SKOS)

g.bind("askomics", rdflib.Namespace("http://askomics.org/internal/"))
g.bind("soa", rdflib.Namespace("https://semopenalex.org/ontology/"))


abstractionGraph = rdflib.Graph()
abstractionGraph.bind("rdf", rdflib.namespace.RDF)
abstractionGraph.bind("rdfs", rdflib.namespace.RDFS)
abstractionGraph.bind("owl", rdflib.namespace.OWL)
abstractionGraph.bind("xsd", rdflib.namespace.XSD)
abstractionGraph.bind("void", rdflib.namespace.VOID)

abstractionGraph.bind("dcterms", rdflib.Namespace("http://purl.org/dc/terms/"))
abstractionGraph.bind("foaf", rdflib.Namespace("http://xmlns.com/foaf/0.1/"))
abstractionGraph.bind("prov", rdflib.Namespace("http://www.w3.org/ns/prov#"))
abstractionGraph.bind("sh", rdflib.Namespace("http://www.w3.org/ns/shacl#"))
abstractionGraph.bind("skos", rdflib.namespace.SKOS)

abstractionGraph.bind("askomics", rdflib.Namespace("http://askomics.org/internal/"))
abstractionGraph.bind("soa", rdflib.Namespace("https://semopenalex.org/ontology/"))

abstractionGraph.add((rdflib.BNode(), rdflib.URIRef("http://www.w3.org/ns/prov#atLocation"), rdflib.Literal("https://semopenalex.org/sparql")))

#g.parse("file:///home/olivier/ontology/semOpenAlex/semopenalex-ontology-shacl20240606.ttl")
#g.parse("semopenalex-ontology-shacl20240606.ttl")
#g.parse("semopenalex-ontology-20240610.ttl")
#
g.parse("semopenalex-ontology-shacl-20250206.ttl")
g.parse("semopenalex-ontology-shacl-20250206-FIX_OD.ttl")

query = """
CONSTRUCT {
  ?class rdf:type askomics:entity .
  ?class rdf:type owl:Class .
  ?class rdf:type askomics:startPoint .
  ?class rdfs:label ?classLabel .
  ?class askomics:instancesHaveNoLabels true .
}
#SELECT ?class ?classLabel
WHERE {
    ?class rdf:type owl:Class .
    ?class rdfs:label ?classLabel .
    FILTER (?class != skos:Concept)
}
"""

qresult = g.query(query)
for row in qresult:
    #print("--->" + "\t".join(row))
    abstractionGraph.add(row)
    #print()
#print(qresult)
#print(str(qresult))



query = """
CONSTRUCT {
[ rdf:type owl:DatatypeProperty ;
  rdfs:label ?propLabel ;
  rdfs:domain ?propDomain ;
  rdfs:range ?propRange ;
  askomics:uri ?dataProp ]
}
WHERE {
  ?dataProp rdf:type owl:DatatypeProperty .
  ?dataProp rdfs:label ?propLabel .
  ?dataProp rdfs:domain ?propDomain .
  
  FILTER (?propDomain != skos:Concept)
  
  ?dataProp rdfs:range ?propRange .
  ?propDomain rdfs:label ?domainLabel .
}
"""
qresult = g.query(query)
for row in qresult:
    #print("--->" + "\t".join(row))
    abstractionGraph.add(row)
    #print()



query = """
CONSTRUCT {
[ rdf:type owl:DatatypeProperty ;
  rdfs:label ?propLabel ;
  rdfs:domain ?propDomain ;
  rdfs:range ?propRange ;
  askomics:uri ?dataProp ]
}
WHERE {
  #VALUES ?dataProp { <https://semopenalex.org/ontology/worksCount> }

  ?shape rdf:type sh:NodeShape .
  ?shape sh:targetClass ?propDomainDirect .
  ?propDomain rdfs:subClassOf* ?propDomainDirect .
  FILTER (?propDomain != skos:Concept)
  
  ?shape sh:property [
    # rdf:type sh:PropertyShape ; # 2025-02-21: explicit typing seems inconsistent in the current version of the ontology
    sh:path ?dataProp ;
    sh:datatype ?propRange
  ] .
  ?dataProp rdf:type owl:DatatypeProperty .
  ?dataProp rdfs:label ?propLabel .

  #?dataProp rdf:type owl:DatatypeProperty .
  #?dataProp rdfs:label ?propLabel .
  #?dataProp rdfs:domain ?propDomain .
  #?dataProp rdfs:domain [ rdf:type owl:Class;
  #    owl:unionOf/(rdf:rest*)/rdf:first ?propDomain ] .
  #?dataProp rdfs:range ?propRange .
}
"""
qresult = g.query(query)
for row in qresult:
    #print("--->" + "\t".join(row))
    abstractionGraph.add(row)
    #print()






query = """
CONSTRUCT {
[ askomics:uri ?objProp ;
  a askomics:AskomicsRelation ;
  a owl:ObjectProperty ;
  rdfs:label ?propLabel ;
  rdfs:domain ?propDomain ;
  rdfs:range ?propRange ]
}
WHERE {
  #VALUES ?objProp { <https://semopenalex.org/ontology/hasConcept> }

  ?shape rdf:type sh:NodeShape .
  ?shape sh:targetClass ?propDomain .
  
  FILTER (?propDomain != skos:Concept)
  
  ?shape sh:property [
    # rdf:type sh:PropertyShape ; # 2025-02-21: explicit typing seems inconsistent in the current version of the ontology
    sh:path ?objProp ;
    sh:class ?propRange ;
  ] .
  ?objProp rdf:type owl:ObjectProperty .
  #
  ?objProp rdfs:label ?propLabel .
  FILTER langMatches( lang(?propLabel), "en" ) # 2025-02-21: inconsistent language tagging in the current version of the ontology
}
"""
qresult = g.query(query)
for row in qresult:
    print("--->" + "\t".join(row))
    abstractionGraph.add(row)
    print()


with open("semopenalex-ontology-shacl-abstraction.ttl", "w") as abstractionFile:
	abstractionFile.write(abstractionGraph.serialize(format='turtle'))

