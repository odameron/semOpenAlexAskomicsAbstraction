# SemOpenAlexAskomicsAbstraction

[SemOpenAlex](https://semopenalex.org) is a 26 billion RDF triple version of the OpenAlex database about scientific publications.
This project generates the abstraction file for using [AskOmics](https://askomics.org/) as an user-friendly front-end for designing SPARQL queries visually.

The abstraction is generated from the SemOpenAlex ontology, retrieved from [SemOpenAlex github repository at https://github.com/metaphacts/semopenalex/tree/main/ontologies](https://github.com/metaphacts/semopenalex/tree/main/ontologies)

