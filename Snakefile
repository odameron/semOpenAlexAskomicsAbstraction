#
# Generate all graphs
# snakemake --forcerun generateWorkflowGraph


rule all:
	input: 'semopenalex-ontology-shacl-abstraction.ttl'


rule generateAskomicsAbstraction:
	input: 'semopenalex-ontology-shacl-20250206.ttl', 'abstractionGenerator-semOpenAlex-shacl.py'
	output: 'semopenalex-ontology-shacl-abstraction.ttl'
	shell:'''
python3 abstractionGenerator-semOpenAlex-shacl.py
# temporary fix for askomics
sed -i 's/xsd:integer/xsd:string/g' semopenalex-ontology-shacl-abstraction.ttl
sed -i 's/xsd:float/xsd:string/g' semopenalex-ontology-shacl-abstraction.ttl
sed -i 's/xsd:numeric/xsd:string/g' semopenalex-ontology-shacl-abstraction.ttl
sed -i 's/xsd:anyURI/xsd:string/g' semopenalex-ontology-shacl-abstraction.ttl
# temporary fix for SemOpenAlex
sed -i 's!http://prismstandard.org/namespaces/basic/2.0/doi!http://purl.org/spar/datacite/doi!g' semopenalex-ontology-shacl-abstraction.ttl
'''


rule generateWorkflowGraphRulegraph:
	input: 'Snakefile'
	output: 'workflow-rule.png'
	#message: "Generating graph..."
	shell: 'snakemake --forceall --rulegraph | dot -Tpng -o {output}'


rule generateWorkflowGraphFilegraph:
	input: 'Snakefile'
	output: 'workflow-file.png'
	#message: "Generating graph..."
	shell: 'snakemake --forceall --filegraph | dot -Tpng -o {output}'


rule generateWorkflowGraph:
	input: 'workflow-dag.png', 'workflow-rule.png', 'workflow-file.png'
	#message: "Generating graph..."
